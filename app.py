from flask import Flask

from views.url_shortener_api import URLShortenerAPI


if __name__ == '__main__':
    app = Flask(__name__)
    url_view = URLShortenerAPI.as_view('url_view')
    app.add_url_rule('/', view_func=url_view, methods=['POST',])
    app.add_url_rule('/<short_url_slug>', view_func=url_view,
                 methods=['GET'])
    app.run(debug=True)
