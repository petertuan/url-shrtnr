import sqlite3
from sqlite3 import OperationalError

def setup_short_urls_and_map():
    create_table = """
    CREATE TABLE SHORT_URLS(
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    SHORT_URL_SLUG TEXT NOT NULL UNIQUE,
    ORIGINAL_URL TEXT
    );
    """
    with sqlite3.connect('urls.db') as conn:
        # Dictionary mapping short_url to original_url
        conn.row_factory = lambda c, r: dict(zip([col[1] for col in c.description], r))

        cursor = conn.cursor()
        try:
            cursor.execute(create_table)
        except OperationalError as e:
            pass

        res = cursor.execute("SELECT * FROM SHORT_URLS")
        short_url_map = res.fetchall()
        return short_url_map
