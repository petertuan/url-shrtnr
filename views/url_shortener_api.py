import random
import sqlite3

try:
    from urllib.parse import urlparse  # Python 3
    str_encode = str.encode
except ImportError:
    from urlparse import urlparse  # Python 2
    str_encode = str

from flask import Flask, abort, jsonify, request, redirect
from flask.views import MethodView

from utils.setup import setup_short_urls_and_map

HOST = 'http://localhost:5000/'
short_url_map = setup_short_urls_and_map()


class URLShortenerAPI(MethodView):
    URL_CHAR = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789'
    NUM_CHARACTERS = 5

    def get(self, short_url_slug):
        """
        Retrieve original url from slug and redirect
        """
        if short_url_slug is not None:
            if short_url_slug in short_url_map:
                original_url = short_url_map[short_url_slug]
            else:
                with sqlite3.connect('urls.db') as conn:
                    cursor = conn.cursor()
                    result = cursor.execute('SELECT ORIGINAL_URL FROM SHORT_URLS WHERE SHORT_URL_SLUG=?', [short_url_slug])
                    try:
                        original_url = result.fetchone()
                        if original_url is None:
                            abort(404)
                    except Exception as e:
                        print(e)
            return redirect(original_url[0])

    def post(self):
        """
        Accepts a long URL, returns a short link
        """
        if request.method == 'POST':
            original_url = request.form.get('original_url')
            if original_url is not None:
                if urlparse(original_url).scheme == '':
                    url = 'http://' + original_url
                else:
                    url = original_url

                while True:
                    short_url_slug = self._generate_slug()
                    if short_url_slug not in short_url_map:
                        break

                with sqlite3.connect('urls.db') as conn:
                    conn.cursor().execute(
                        'INSERT INTO SHORT_URLS (SHORT_URL_SLUG, ORIGINAL_URL) VALUES (?, ?)',
                        [short_url_slug, url]
                    )

                short_url_map[short_url_slug] = {original_url: original_url}

                short_url = HOST + short_url_slug
                return jsonify({'short_url': short_url})
            else:
                return abort(400)
    
    def _generate_slug(self):
        """
        Generate random string from available characters
        """
        slug = ''
        for _ in range(self.NUM_CHARACTERS):
            slug += random.choice(self.URL_CHAR)

        return slug
