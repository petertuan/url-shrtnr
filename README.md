# URL Shrtnr
This is a simple Flask application built with two endpoints - one for generating a short link from a given URL and the other for retrieving the original URL and redirecting to it.

## Installation and Startup
With Python and pip installed on your system, run the following:
`pip install -r requirements.txt`
`python app.py`

To create a new short link, send a POST REQUEST to http://localhost:5001 with a key of 'original_url' and a value of the link you want shortened, and it will return a JSON with the short_url.

## Approach
I began by looking at the goo.gl URL shortener to get an idea of how one works in practice. In particular, I noted a couple things: 

1) The same URL can be supplied multiple times by many different users and should result in a new, unique short link
2) Tracking analytics is provided for each link, which helps explain the above requirement, and a good design would allow for easy expansion to include this feature even if not initially requested.

### Brainstorming solutions
The first thought that came to mind was a hash map of some sort, from a string (the long URL) to a unique hash. However, this would be inadequate to satisfy the above requirements, where by definition of a hash, a long URL input would always output the same short link.

A sensible solution then seemed to be to use a persistent store, such as a DB table with schema:
id - primary identifier, auto-generated
short_link - used for look-ups, therefore should have an index
original_url - store for redirection

One approach is to then pre-generate all the possible combinations using 4-6 characters, using valid characters and digits while excluding those that may not be so user-friendly or PG. Then for every long URL input, we randomly select one short URL that is available, update its row with the original_url, and now that is available for lookup on short URL visit.

Another approach and the one I will use here is to add urls as we go. On app initialization, we load all taken urls into memory into a Python dictionary for fast lookup (on a real system this could go into a distributed cache such as Redis for all our instances to access), and then for each new URL to generate, we randomly create a 5-character string, check if it already exists in the dictionary, and if not, add to the dictionary/cache and database.

## Application Structure
This is a simple Flask app that uses SQLite to demonstrate a database table prepopulated with 5 character combinations from a URL-friendly character set (a-z, A-Z, 1-9). A POST with a url will return a short link. A GET will look up a short link, if not found give a 404 response, and redirect the user to the original_url.

## Future optimizations
To scale out this service, one could optimize with an in-memory cache such as Redis of taken short link URLs. A request would then quickly be answered instead of having to access the persistent store. We could have a daily cron job checking the availability of unused URLs (# of taken URLs / possible permutations we allow to get a % full indicator) and when a certain threshold is crossed, either up the character amount or purge based off some criteria (usually not recommended with how long stuff lives on in the web). Lastly, if we are to be at such scale or to be constrained by memory, we could implement a Bloom filter instead to check if a randomly generated slug exists yet or not to reduce the number of accesses to the persistent store.
